import React from 'react'
import { isLight } from '../Helper/helper'

function WithStyle(Component) {
	return function addTheme({ ...props }) {
		return <Component {...props} className={isLight() ? 'light' : 'dark'} />
	}
}
export default WithStyle
