import { useGlobal } from '../Context/GlobalContext'

export function isLight() {
	const { theme } = useGlobal()
	return theme == 'Light'
}
