import React from 'react'
import PropTypes from 'prop-types'
import WithStyle from '../HOC/WithStyle'

class ErrorBoundary extends React.Component {
	constructor(props) {
		super(props)
		this.state = { error: null, errorInfo: null }
	}
	static getDerivedStateFromError(error) {
		return { error: error }
	}

	componentDidCatch(error, errorInfo) {
		this.setState({
			error: error,
			errorInfo: errorInfo,
		})
	}

	render() {
		if (this.state.errorInfo) {
			return (
				<div
					style={{
						height: 'calc(100vh - 56px)',
						display: 'flex',
						flexDirection: 'column',
						justifyContent: 'center',
						alignItems: 'center',
						transition: 'all 0.2s ease-in',
					}}
					className={this.props.className}
				>
					<h2>{this.state.error && this.state.error.toString()}</h2>
					<button
						className={`btn ${this.props.className}`}
						onClick={() => this.setState({ error: null, errorInfo: null })}
					>
						reset
					</button>
				</div>
			)
		}
		return this.props.children
	}
}
ErrorBoundary.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
}
export default WithStyle(ErrorBoundary)
