import React, { Fragment, lazy, Suspense } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Navbar from './Components/Navbar/Navbar'
import ErrorBoundary from './ErrorBoundaries/ErrorBoundary'
const Home = lazy(() => import('./Pages/Home'))
import './App.scss'

function App() {
	return (
		<Fragment>
			<Navbar />
			<BrowserRouter>
				<Routes>
					<Route
						path='/'
						element={
							<ErrorBoundary>
								<Suspense fallback={<h1>Loading Component...</h1>}>
									<Home />
								</Suspense>
							</ErrorBoundary>
						}
					/>
				</Routes>
			</BrowserRouter>
		</Fragment>
	)
}

export default App
