import React, { createContext, useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
const Global = createContext()
function GlobalContext({ children }) {
	const [theme, setTheme] = useState(localStorage.getItem('theme') || 'Light')
	useEffect(() => {
		localStorage.setItem('theme', theme)
	}, [theme])
	return (
		<Global.Provider value={{ theme, setTheme }}>{children}</Global.Provider>
	)
}

export function useGlobal() {
	return useContext(Global)
}

GlobalContext.propTypes = {
	children: PropTypes.node,
}
export default GlobalContext
