import React from 'react'
import PropTypes from 'prop-types'
import './button.scss'

function Button({ children, className, ...props }) {
	return (
		<button className={`btn ${className}`} {...props}>
			{children}
		</button>
	)
}

Button.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
}
export default Button
