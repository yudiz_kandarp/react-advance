import React from 'react'
import PropTypes from 'prop-types'
import { useGlobal } from '../../Context/GlobalContext'
import WithStyle from '../../HOC/WithStyle'
import Sun from '../../Assets/day.svg'
import Moon from '../../Assets/moon.svg'
import { isLight } from '../../Helper/helper'
import './navbar.scss'

function Navbar({ className }) {
	const { setTheme } = useGlobal()
	return (
		<header className={className}>
			<div className={`navbar_container`}>
				<h3>Home</h3>
				<nav>
					<ul>
						<li
							tabIndex={0}
							role='button'
							aria-label='theme'
							aria-expanded={true}
							onClick={() =>
								setTheme((prev) => (prev == 'Light' ? 'Dark' : 'Light'))
							}
						>
							{isLight() ? (
								<img src={Moon} alt='dark' />
							) : (
								<img src={Sun} alt='light' />
							)}
						</li>
					</ul>
				</nav>
			</div>
		</header>
	)
}

Navbar.propTypes = {
	className: PropTypes.string,
}

export default WithStyle(Navbar)
