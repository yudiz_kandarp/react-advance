import React, { useState, forwardRef, useImperativeHandle } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import './modal.scss'
import WithStyle from '../../HOC/WithStyle'

const Modal = WithStyle(function Modal({ children, className, ModalRef }) {
	const [show, setShow] = useState(false)
	useImperativeHandle(ModalRef, () => {
		return {
			OpenModal,
			CloseModal,
		}
	})
	function OpenModal() {
		setShow(true)
	}
	function CloseModal() {
		setShow(false)
	}

	if (show) {
		return ReactDOM.createPortal(
			<div className={`modal_wrapper ${className}`}>
				<div onClick={CloseModal} className='modal_backdrop' />
				<div className={`modal_box ${className}`}>{children}</div>
			</div>,
			document.getElementById('root-modal')
		)
	}
	return null
})

Modal.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
}
export default forwardRef(function forwardedRef(props, ref) {
	return <Modal {...props} ModalRef={ref} />
})
