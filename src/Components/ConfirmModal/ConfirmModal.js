import React from 'react'
import PropTypes from 'prop-types'
import Button from '../Button/Button'
import WithStyle from '../../HOC/WithStyle'
import './confirmModal.scss'

function ConfirmModal({ onClose, className }) {
	return (
		<div className='confirm_modal'>
			<h2>Are You sure?</h2>
			<div className='btn_container'>
				<Button className={className} onClick={onClose}>
					cancel
				</Button>
				<Button className={className} onClick={onClose}>
					Confirm
				</Button>
			</div>
		</div>
	)
}
ConfirmModal.propTypes = {
	onClose: PropTypes.func,
	className: PropTypes.string,
}

export default WithStyle(ConfirmModal)
