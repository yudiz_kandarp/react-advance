import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'
import WithStyle from '../HOC/WithStyle'
import Modal from '../Components/Modal/Modal'
import Button from '../Components/Button/Button'
import ConfirmModal from '../Components/ConfirmModal/ConfirmModal'
import './home.scss'

function Home({ className }) {
	const [count, setCount] = useState(0)
	const ModalRef = useRef(null)
	if (count > 10) throw new Error('Maximum Limit Reached')
	if (count < 0) throw new Error('Minimum Limit Reached')
	return (
		<>
			<div className={`home dark ${className}`}>
				<>
					<Button
						className={className}
						onClick={() => setCount((prev) => prev - 1)}
					>
						decrement -
					</Button>
					<div className={`count dark ${className}`}>{count}</div>
					<Button
						className={className}
						onClick={() => setCount((prev) => prev + 1)}
					>
						increment +
					</Button>
				</>

				<Button onClick={() => ModalRef.current.OpenModal()}>open Modal</Button>
			</div>
			<Modal ref={ModalRef}>
				<ConfirmModal onClose={() => ModalRef.current.CloseModal()} />
			</Modal>
		</>
	)
}

Home.propTypes = {
	className: PropTypes.string,
}

export default WithStyle(Home)
